<?php
namespace Rubeus\Servicos\Email;  
use Rubeus\ContenerDependencia\Conteiner;

class EnvioEmailArq{
    private $mail;
    private $config;
    
    public function __construct($id=false){
        if($id != false){
            $this->iniciar($id);
        }
    }
    
    public function iniciar($id){
        $this->config = Conteiner::get('EnvioEmailConfig');
        $this->mail = new Email($this->config->{$id});
    }
    
    public function alterarEmailEnvio($destino){
        return $this->mail->setDestino($destino);
    }
    
    public function setArquivo($arquivo){
        $this->mail->setArquivo($arquivo);
    }
    
    public function enviarEmail($tipo, $enderecoEmail, $array){        
        ob_start();
        include DIR_BASE.'/'.$this->config->{$tipo};
        $texto = ob_get_contents();
        ob_end_clean();
        
        $this->mail->setTexto($texto);
        $this->mail->setAssunto($array['assunto']);
        $this->mail->setDestino($enderecoEmail);
        
        if(isset($array['emailResposta'])){
            $this->mail->setEmailResposta($array['emailResposta']);
        }
        return $this->mail->enviar();         
    }
    
    public function getErro(){
        return $this->mail->getErroCodigo();
    }
    
}
