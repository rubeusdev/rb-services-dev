<?php
namespace Rubeus\Servicos\Vetor;

abstract class VetorAssociativo{
    
    public static function get(&$dados, $string, $valorPadrao=false){
        if(is_string($string))
            $var = explode(',',$string);
        else $var = $string;
        $aux = $dados;
        foreach($var as $v){
            if(isset($aux[$v])){
                $aux = $aux[$v];
            }else return $valorPadrao;
        }
        return $aux;
    }
    
    public static function set(&$dados, $valor, $string=false){
        if($string === false){
            $dados = $valor;
            return ;
        }
        $var = explode(',',$string);
        if(is_array($var)){
            if(count($var)==1)$dados[$var[0]] = $valor;            
            else if(count($var)==2)$dados[$var[0]][$var[1]] = $valor;            
            else if(count($var)==3)$dados[$var[0]][$var[1]][$var[2]] = $valor;
            else if(count($var)==4)$dados[$var[0]][$var[1]][$var[2]][$var[3]] = $valor;
            else if(count($var)==5)$dados[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]] = $valor;
        }else $dados[$var] = $valor;        
    }

    public static function limpar(&$dados, $string = false){
        if($string === false){
            $dados = null;
            return ;
        }
       
        $var = explode(',',$string);

        if(is_array($var)){
            if(count($var)==1)unset($dados[$var[0]]);            
            else if(count($var)==2)unset($dados[$var[0]][$var[1]]);            
            else if(count($var)==3)unset($dados[$var[0]][$var[1]][$var[2]]);
            else if(count($var)==4)unset($dados[$var[0]][$var[1]][$var[2]][$var[3]]);
            else if(count($var)==5)unset($dados[$var[0]][$var[1]][$var[2]][$var[3]][$var[4]]);
        }else unset($dados[$var]);        
    }
    
    
}

