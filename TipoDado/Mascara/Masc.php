<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

abstract class Masc{
    static $separador='x';
    
    public static function removerMascara(&$valor, $mascara){
        $caracteres = str_split(str_replace(self::$separador , "", $mascara));
        if(is_array($valor)){
            $num = count($valor);
            for($i=0; $i < $num; $i++) $valor[$i] = str_replace($caracteres , "", $valor[$i]);
        }else $valor = str_replace($caracteres , "", $valor); 
    }
 	
    public static function colocarMascara(&$valor, $mascara){
        $mascara = str_split($mascara);
        if(is_array($valor)){
            $num= count($valor);
            for($i=0; $i < $num; $i++)
                $valor[$i] = self::colocaMascara($valor[$i], $mascara);
        }else $valor = self::colocaMascara($valor, $mascara);
    }
    
    private static function colocaMascara($valor, $mascara){
        $valor = str_split($valor);
        $retorno = '';
        $j=-1;
        foreach ($mascara as $m)
            $retorno .= $m==self::$separador ? $valor[++$j] : $m; 
        return $retorno;
    }
}