<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

abstract  class TipoMasc {
    protected $valor;
    protected $masc;
       
    public function iniciar($masc, $valor=null) {
        $this->masc = $masc;
        if(!is_null($valor))$this->set($valor);
    }
    
    public function set($valor){
        Masc::removerMascara($valor, $this->masc);        
        $this->valor  = $valor;      
        return $this;
    }
    
    public function setValidar($valor){
        Masc::removerMascara($valor, $this->masc);        
        $this->valor  = $valor;
        $this->validar();        
        return $this->valor;
    }
    
    public function get(){
        return $this->valor ;
    }

    public function removerMascara(){
        if($this->valor)Masc::removerMascara($this->valor, $this->masc);
        return $this;
    }
 	
    public function colocarMascara(){
        if($this->valor)Masc::colocarMascara($this->valor, $this->masc);
        return $this;
    }
    
    abstract public function validar();
}