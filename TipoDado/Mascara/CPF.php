<?php
namespace Rubeus\Servicos\TipoDado\Mascara;

class CPF extends TipoMasc{

    public function __construct($valor=null) {
        $this->iniciar('xxx.xxx.xxx-xx',$valor);
    }

    public function validar(){
        $nulos = array("12345678909","11111111111","22222222222","33333333333",
                       "44444444444","55555555555","66666666666","77777777777",
                       "88888888888","99999999999","00000000000");
        //if (!(preg_match("[0-9.-]",$this->valor)))return false;
        if (!(preg_match("/[0-9.-]/",$this->valor))){
            $this->valor=false;
            return false;
        }

        if (!(preg_match("'\d{11}'",$this->valor))){
            $this->valor=false;
            return false;
        }
        $this->valor = preg_replace("[^0-9]", "", $this->valor);
        if( in_array($this->valor, $nulos)) $this->valor = false;
        // Calcula o pen�ltimo d�gito verificador
        $acum=0;
        for($i=0; $i<9; $i++) $acum+= $this->valor[$i]*(10-$i);
        $x=$acum % 11;
        $acum = ($x>1) ? (11 - $x) : 0;
        // Retorna falso se o digito calculado eh diferente do passado na string
        if ($acum != $this->valor[9]) $this->valor = false;
        // Calcula o �ltimo d�gito verificador
        $acum=0;
        for ($i=0; $i<10; $i++) $acum+= $this->valor[$i]*(11-$i);
        $x=$acum % 11;
        $acum = ($x > 1) ? (11-$x) : 0;
        // Retorna falso se o digito calculado eh diferente do passado na string
        if ( $acum != $this->valor[10]) $this->valor = false;

    }

}
