<?php
namespace Rubeus\Servicos\Base64;

class Upload {
    
    public function upload($base64_string, $output_file){
        $ifp = fopen($output_file, "wb");
        
        $data = explode(',', $base64_string);
        if(isset($data[1])){
            $imagem = $data[1];
        }else if(is_array($data)){
            $imagem = $data[0];
        }else{
            $imagem = $data;
        }
        
        fwrite($ifp, base64_decode($imagem));
        fclose($ifp);
        
        return $output_file;
    }
}
