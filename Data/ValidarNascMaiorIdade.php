<?php
namespace Rubeus\Servicos\Data;

class ValidarNascMaiorIdade implements InterfaceValidarData{

    public function validar($data){     	
        $dataAtual = new DataHora();
        $diff = $data->diff($dataAtual);

        if($diff->days > 0 && $diff->y < 150 && $diff->y >=18 && $data < $dataAtual){            
            return 1;
        }else{            
            if ($diff->y < 18) {                            
        		return 2;
        	}
        	return 0;
        }
    }
}
