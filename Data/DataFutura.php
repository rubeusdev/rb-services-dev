<?php
namespace Rubeus\Servicos\Data;

class DataFutura implements InterfaceValidarData{

    public function validar($data){
        $dataAtual = new \DateTime();
        $diff = $data->diff($dataAtual);
        if($diff->d > 0 && $data > $dataAtual) return true;
        return false;
    }

}
