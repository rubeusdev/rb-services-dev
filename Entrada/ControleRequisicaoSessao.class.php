<?php
namespace controle\biblioteca;
use principal\Saida as Saida;
use controle\control\R as R;

abstract class ControleRequisicaoSessao{ 
    
    public  static function permitir($metodo){ 
        if(!Sessao::get('persistente,controleReq,'.$metodo.',tentativas')){
            Sessao::set('persistente,controleReq,'.$metodo.',tentativas',0);
            Sessao::set('persistente,controleReq,'.$metodo.',incioEnvio',date('Y-m-d H:i:s'));
            Sessao::set('persistente,controleReq,'.$metodo.',ultimoEnvio',date('Y-m-d H:i:s'));
        }
        Sessao::set('persistente,controleReq,'.$metodo.',tentativas',
                    Sessao::get('persistente,controleReq,'.$metodo.',tentativas')+1);
        $diferenca = strtotime(date('Y-m-d H:i:s'))
                -strtotime(Sessao::get('persistente,controleReq,'.$metodo.',ultimoEnvio'));
        $indice = Sessao::get('persistente,controleReq,'.$metodo.',tentativas')/
                    (strtotime(date('Y-m-d H:i:s'))-strtotime(Sessao::get('persistente,controleReq,'.$metodo.',incioEnvio')));
        Sessao::set('persistente,controleReq,'.$metodo.',ultimoEnvio',date('Y-m-d H:i:s'));
        if(is_null($indice) || !$indice || ($diferenca > 3 && ($_SESSION['tentativas'] < 3 || floatval($indice) < 0.01)))
            return true;
        Saida::ecoar(R::estRet(false, "va_calma"));
    }
    
   
}