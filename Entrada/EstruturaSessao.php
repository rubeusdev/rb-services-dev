<?php
namespace Rubeus\Servicos\Entrada;
use Rubeus\Servicos\Json\Json;
use Rubeus\Servicos\Emcrypt\Emcrypt;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\Entrada\Sessao;

class EstruturaSessao{
    private $sessaoBanco;

    public function __construct() {
        $this->sessaoBanco = new Estrutura\SessaoBanco();
    }

    public function restaurarSessao($idSessao){
        $this->sessaoBanco->limparObjeto();
        $this->sessaoBanco->setCodigo($idSessao);
        if($idSessao!==false){
            $this->sessaoBanco->carregar();
        }
        if($this->sessaoBanco->getId() && $idSessao
                && $this->sessaoBanco->getCodigo() == $idSessao){
            return array(   "codSess" => $this->sessaoBanco->getCodigo(),
                            "idSess" => $this->sessaoBanco->getId(),
                            "dadosSessao" => Json::lerJsonComoArray($this->sessaoBanco->getDadosSessao())
                    );
        }
        $this->sessaoBanco->limparObjeto();
        $this->sessaoBanco->setMomento(Conteiner::get('DataHora',false)->get('Y-m-d H:i:s'));
        $this->sessaoBanco->setAtivo(1);
        if($idSessao){
            $this->sessaoBanco->setCodigo($idSessao);
            $this->sessaoBanco->salvar();
        }else{
            $codigo = Emcrypt::gerarCodigoBanco(new Estrutura\SessaoBanco(),'codigo', 100, array());
            $this->sessaoBanco->setCodigo($codigo);
            $this->sessaoBanco->salvar();
        }
        return array(   "codSess" => $this->sessaoBanco->getCodigo(),
                        "idSess" => $this->sessaoBanco->getId(),
                        "dadosSessao" => []
                );
    }

    public function getSessaoBanco(){
        return $this->sessaoBanco;
    }

    public function registrarSessao(){
        if(Sessao::get('idSess')){
            $this->sessaoBanco->setId(Sessao::get('idSess'));
        }else{
            $codigo = Emcrypt::gerarCodigoBanco(new Estrutura\SessaoBanco(),'codigo', 100, array());
            $this->sessaoBanco->setCodigo($codigo);
        }

        $this->sessaoBanco->setDadosSessao(json_encode(Sessao::getSessao()));
        $this->sessaoBanco->salvar();
        Sessao::set('idSess', $this->sessaoBanco->getId());
        return $this->sessaoBanco->getId();
    }

    public function getCodigoSessao($id){
        if($id){
            $this->sessaoBanco->limparObjeto();
            $this->sessaoBanco->setId($id);
            $codigoAtual = $this->sessaoBanco->carregar('codigo', false, '{codigo}');

            if($codigoAtual){
                return $codigoAtual;
            }
        }
        $codigo = Emcrypt::gerarCodigoBanco(new Estrutura\SessaoBanco(),'codigo', 100, array());
        $this->sessaoBanco->setCodigo($codigo);
        $this->sessaoBanco->salvar();
        Sessao::set('idSess', $this->sessaoBanco->getId());
        return $codigo;
    }
}
