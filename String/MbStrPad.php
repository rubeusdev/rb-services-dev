<?php

namespace Rubeus\Servicos\String;

class MbStrPad{
	/**
	 * [MONTA A LINHA PARA GRAVAR ARQUIVO CSV]
	 * @param  [type] $str      [description]
	 * @param  [type] $pad_len  [description]
	 * @param  string $pad_str  [description]
	 * @param  [type] $dir      [description]
	 * @param  [type] $encoding [description]
	 * @return [string]           [retorno da funcao]
	 */
	public function mb_str_pad($str=NULL, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT, $encoding = NULL){
        $str = substr($str,0,$pad_len);
        $encoding = $encoding === NULL ? mb_internal_encoding() : $encoding;
        $padBefore = $dir === STR_PAD_BOTH || $dir === STR_PAD_LEFT;
        $padAfter = $dir === STR_PAD_BOTH || $dir === STR_PAD_RIGHT;
        $pad_len -= mb_strlen($str === NULL ? '' : $str, $encoding);
        $targetLen = $padBefore && $padAfter ? $pad_len / 2 : $pad_len;
        $strToRepeatLen = mb_strlen($pad_str, $encoding);
        $repeatTimes = ceil($targetLen / $strToRepeatLen);
        $repeatedString = str_repeat($pad_str, max(0, $repeatTimes)); // safe if used with valid unicode sequences (any charset)
        $before = $padBefore ? mb_substr($repeatedString, 0, floor($targetLen), $encoding) : '';
        $after = $padAfter ? mb_substr($repeatedString, 0, ceil($targetLen), $encoding) : '';

        return $before . $str . $after;
    }

}