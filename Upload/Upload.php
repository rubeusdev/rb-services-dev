<?php
namespace Rubeus\Servicos\Upload;
use Rubeus\Servicos\Emcrypt\Emcrypt;
use Rubeus\ContenerDependencia\Conteiner;

class Upload{
    private $erro;

    public function getErro(){
        return $this->erro;
    }

    public function upar($dados,$atributo,$pasta){
        $this->erro = false;
        if (!empty($dados)) {

            $config = Conteiner::get('CONFIG_UPLOAD');
            if(!is_array($dados['tmp_name'])){
                $dados['tmp_name'] = array($dados['tmp_name']);
                $dados['name'] = array($dados['name']);
            }
            $arquivos = array();
            $qtd = count($dados['tmp_name']);
            for($i = 0; $i < $qtd; $i++){
                $tipo = explode('.', $dados['name'][$i]);
                if(defined('DIR_FILE_INSTITUICAO')){
                    $diretorioConf = str_replace('file/', 'file/'.DIR_FILE_INSTITUICAO.'/', $config->$atributo->diretorio);
                    $urlConf = str_replace('file/', 'file/'.DIR_FILE_INSTITUICAO.'/', $config->$atributo->url);
                }else{
                    $diretorioConf = $config->$atributo->diretorio;
                    $urlConf = $config->$atributo->url;
                }

                mkdir($diretorioConf .'/'.$pasta,0777,true);

                $imagem =  Emcrypt::codigo(3).'-'.date('Y-m-d-H-i-s').'.'.$tipo[count($tipo)-1];

                $endereco = $diretorioConf .'/'.$pasta.'/'.$imagem;
                $url = $urlConf .'/'.$pasta.'/'. $imagem;

                if(!move_uploaded_file($dados['tmp_name'][$i], $endereco )){
                    $this->erro = ['codErro' => 'fal_arq', 'arquivo' => $dados['tmp_name'][$i]];
                    return false;
                }else{
                    $arquivos[] = ['endereco' => $endereco, 'url' => $url] ;
                }
            }
            return $arquivos;
        }
        $this->erro = ['codErro' => 'er_sem_arq'];
        return false;
    }
}